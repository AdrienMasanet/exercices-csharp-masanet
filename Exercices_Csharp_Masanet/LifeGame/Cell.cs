namespace Masanet_console_1.LifeGame {
    public class Cell {
        private Vector2 _transform;

        public Cell(int x, int y, bool isAlive = false) {
            _transform = new Vector2(x, y);
            IsAlive = isAlive;
            WillBeAlive = isAlive;
        }

        public Vector2 Transform {
            get => _transform;
            set => _transform = value ?? throw new ArgumentNullException(nameof(value));
        }

        public bool IsAlive { get; set; }
        public bool WillBeAlive { get; set; }
    }
}