namespace Masanet_console_1.LifeGame {
    public static class LifeGame {
        private static Map? _map;

        public static void Start() {
            Console.Clear();

            var random = new Random();
            int randomMapWidth = random.Next(15, 35);
            int randomMapHeight = random.Next(10, 20);
            int cellCount;

            Console.WriteLine("Combien de cellules voulez-vous instancier ?");
            string? input = Console.ReadLine();
            if (input != null) {
                try {
                    cellCount = int.Parse(input);

                    if (cellCount <= 0) {
                        Console.WriteLine("Erreur : l'entrée est incorrecte." +
                                          "\n" +
                                          "L'entier ne peut pas être négatif.");
                        return;
                    }
                }
                catch (Exception e) {
                    Console.WriteLine("Erreur : l'entrée est incorrecte." +
                                      "\n" +
                                      "Vous devez entrer un entier valide.");
                    return;
                }
            }
            else {
                Console.WriteLine("Erreur : l'entrée est incorrecte." +
                                  "\n" +
                                  "Vous devez entrer un entier valide.");
                return;
            }

            _map = new Map(50, 50);
            _map.GenerateWorld();
            _map.GenerateCells(cellCount);

            while (true) {
                Console.CursorVisible = false;
                _map.DrawCells();
                _map.UpdateCells();
            }
        }
    }
}