namespace Masanet_console_1.LifeGame {
    public class Map {
        private readonly int _tilesXCount;
        private readonly int _tilesYCount;
        private readonly List<Cell> _cells = new List<Cell>();

        public Map(int tilesXCount, int tilesYCount) {
            _tilesXCount = tilesXCount;
            _tilesYCount = tilesYCount;
        }

        public void GenerateWorld() {
            for (int y = 0; y < _tilesYCount; y++) {
                for (int x = 0; x < _tilesXCount; x++) {
                    if (y >= 0 && x >= 0 && y != _tilesYCount && x != _tilesXCount) {
                        _cells.Add(new Cell(x, y, false));
                    }
                }
            }
        }

        public void GenerateCells(int cellAmount) {
            int addedCells = 0;
            Random randomWillBeAliveCell = new Random();
            int randomness = (_tilesXCount * _tilesYCount) + 1;

            while (addedCells < cellAmount) {
                foreach (var cell in _cells) {
                    if (randomWillBeAliveCell.Next(randomness) == 1 && addedCells < cellAmount) {
                        cell.IsAlive = true;
                        addedCells++;
                    }
                }
            }
        }

        public void DrawCells() {
            var enumerable = _cells;
            int lastCellY = 0;
            string finalOutput = "";

            Console.Clear();

            foreach (var cell in enumerable) {
                if (cell.Transform.Y > lastCellY) {
                    finalOutput += "\n";
                }

                if (cell.IsAlive) {
                    Random randomWhichColor = new Random();
                    int whichColor = randomWhichColor.Next(8);

                    switch (whichColor) {
                        case 1:
                            finalOutput += "🔴";
                            break;
                        case 2:
                            finalOutput += "🟠";
                            break;
                        case 3:
                            finalOutput += "🟡";
                            break;
                        case 4:
                            finalOutput += "🟢";
                            break;
                        case 5:
                            finalOutput += "🔵";
                            break;
                        case 6:
                            finalOutput += "🟣";
                            break;
                        case 7:
                            finalOutput += "🟤";
                            break;
                    }
                }
                else {
                    finalOutput += "  ";
                }

                lastCellY = cell.Transform.Y;
            }

            Console.Write(finalOutput);
            Console.WriteLine("\n Étape : " + GenerationTurn);
            Thread.Sleep(10);
        }

        public void UpdateCells() {
            GenerationTurn++;

            foreach (var cell in _cells) {
                int adjacentCellsCount = 0;

                if (GetCellAt(cell.Transform.X, cell.Transform.Y - 1) != null &&
                    GetCellAt(cell.Transform.X, cell.Transform.Y - 1).IsAlive) {
                    adjacentCellsCount++;
                }

                if (GetCellAt(cell.Transform.X + 1, cell.Transform.Y - 1) != null &&
                    GetCellAt(cell.Transform.X + 1, cell.Transform.Y - 1).IsAlive) {
                    adjacentCellsCount++;
                }

                if (GetCellAt(cell.Transform.X + 1, cell.Transform.Y) != null &&
                    GetCellAt(cell.Transform.X + 1, cell.Transform.Y).IsAlive) {
                    adjacentCellsCount++;
                }

                if (GetCellAt(cell.Transform.X + 1, cell.Transform.Y + 1) != null &&
                    GetCellAt(cell.Transform.X + 1, cell.Transform.Y + 1).IsAlive) {
                    adjacentCellsCount++;
                }

                if (GetCellAt(cell.Transform.X, cell.Transform.Y + 1) != null &&
                    GetCellAt(cell.Transform.X, cell.Transform.Y + 1).IsAlive) {
                    adjacentCellsCount++;
                }

                if (GetCellAt(cell.Transform.X - 1, cell.Transform.Y + 1) != null &&
                    GetCellAt(cell.Transform.X - 1, cell.Transform.Y + 1).IsAlive) {
                    adjacentCellsCount++;
                }

                if (GetCellAt(cell.Transform.X - 1, cell.Transform.Y) != null &&
                    GetCellAt(cell.Transform.X - 1, cell.Transform.Y).IsAlive) {
                    adjacentCellsCount++;
                }

                if (GetCellAt(cell.Transform.X - 1, cell.Transform.Y - 1) != null &&
                    GetCellAt(cell.Transform.X - 1, cell.Transform.Y - 1).IsAlive) {
                    adjacentCellsCount++;
                }

                switch (adjacentCellsCount) {
                    case < 2:
                        cell.WillBeAlive = false;
                        break;
                    case 2:
                        cell.WillBeAlive = cell.IsAlive;
                        break;
                    case 3:
                        cell.WillBeAlive = true;
                        break;
                    case > 3:
                        cell.WillBeAlive = false;
                        break;
                }
            }

            foreach (var cell in _cells) {
                cell.IsAlive = cell.WillBeAlive;
            }
        }

        public Cell? GetCellAt(int x, int y) {
            foreach (var cell in _cells) {
                if (cell != null && cell.Transform.X == x && cell.Transform.Y == y) {
                    return cell;
                }
            }

            return null;
        }

        public int GenerationTurn { get; set; } = 0;
    }
}