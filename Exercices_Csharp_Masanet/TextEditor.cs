namespace Masanet_console_1 {
    internal static class TextEditor {
        private static List<Character> _currentCharacters = new List<Character>();

        public static void Start() {
            Console.Clear();

            while (true) {
                if (_currentCharacters != null) {
                    foreach (var characterToPrint in _currentCharacters) {
                        Console.ForegroundColor = characterToPrint.GetColor();
                        Console.Write(characterToPrint.GetCharacter());
                    }

                    var pressedKey = Console.ReadKey().KeyChar;

                    if (pressedKey == '') {
                        if (_currentCharacters.Count >= 1) {
                            _currentCharacters.RemoveAt(_currentCharacters.Count - 1);
                        }
                    }
                    else {
                        Random randomGenerator = new Random();
                        int randomResult = randomGenerator.Next(9, 14);
                        ConsoleColor cc = (ConsoleColor) randomResult;
                        Character newCharacter = new Character(cc, pressedKey);
                        _currentCharacters?.Add(newCharacter);
                    }
                }

                Thread.Sleep(5);
                Console.Clear();
            }
        }
    }

    internal class Character {
        private ConsoleColor color;
        private char character;

        public Character(ConsoleColor color, char character) {
            this.color = color;
            this.character = character;
        }

        public char GetCharacter() {
            return this.character;
        }

        public ConsoleColor GetColor() {
            return this.color;
        }
    }
}