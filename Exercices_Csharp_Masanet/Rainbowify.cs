namespace Masanet_console_1 {
    public static class Rainbowify {
        public static void Start() {
            Console.Clear();
            Console.WriteLine("Veuillez écrire quelque chose");
            string? toProcess = Console.ReadLine();

            if (toProcess == null) return;
            Console.CursorVisible = false;

            while (!Console.KeyAvailable) {
                foreach (var character in toProcess) {
                    Random randomGenerator = new Random();
                    int randomResult = randomGenerator.Next(9, 14);
                    ConsoleColor cc = (ConsoleColor) randomResult;
                    Console.ForegroundColor = cc;
                    Console.Write(character);
                }

                Thread.Sleep(100);
                Console.Clear();
            }

            Console.Clear();
            Console.CursorVisible = true;
        }
    }
}