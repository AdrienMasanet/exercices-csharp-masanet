﻿namespace Masanet_console_1 {
    internal static class BlinkingInput {
        private static void Main()
        {
            Console.Clear();
            Console.WriteLine("Écrivez :");
            Console.WriteLine("- 1 pour le texte scintillant");
            Console.WriteLine("- 2 pour l'éditeur de texte");
            Console.WriteLine("- 3 pour le petit rogue like");
            Console.WriteLine("- 4 pour le jeu de la vie");
            string? choice = Console.ReadLine();

            switch (choice)
            {
                case "1":
                    Rainbowify.Start();
                    break;
                case "2":
                    TextEditor.Start();
                    break;
                case "3":
                    RogueLike.RogueLike.Start();
                    break;
                case "4":
                    LifeGame.LifeGame.Start();
                    break;
                default:
                    Console.WriteLine("Vous avez écrit n'importe quoi");
                    break;
            }
        }
    }
}