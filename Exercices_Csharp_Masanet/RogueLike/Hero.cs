namespace Masanet_console_1.RogueLike {
    public class Hero {
        private Vector2 _transform;
        private Map? _map;
        private int _healthPoints;
        private int _maxHealthPoints = 3;
        private string _sprite = "😀";

        public Hero(int startingPosX, int startingPosY) {
            _transform = new Vector2(startingPosX, startingPosY);
            _healthPoints = _maxHealthPoints;
        }

        public Vector2 Transform {
            get => _transform;
            set => _transform = value ?? throw new ArgumentNullException(nameof(value));
        }

        public Map? Map {
            get => _map;
            set => _map = value;
        }

        public void DrawStats() {
            Console.WriteLine();
            Console.WriteLine("Health : " + _healthPoints + "/" + _maxHealthPoints);
        }

        public void AddHealth(int pointsToAdd = 1) {
            if (_healthPoints < _maxHealthPoints) _healthPoints++;
        }

        public void SubHealth(int pointsToSub = 1) {
            if (_healthPoints > 1) {
                _healthPoints--;
            }
            else {
                _healthPoints = 0;
                Masanet_console_1.RogueLike.RogueLike.GameOver();
            }
        }

        public void UpdateSprite() {
            double healthInPercentage = (
                (_healthPoints / (double) _maxHealthPoints) * 100
            );

            if (healthInPercentage == 0d) {
                _sprite = "💀";
            }
            else if (healthInPercentage is not 0d and < 35d) {
                _sprite = "😫";
            }
            else if (healthInPercentage is >= 35d and <= 70d) {
                _sprite = "😐";
            }
            else if (healthInPercentage > 70d) {
                _sprite = "😀";
            }
        }

        public void Move(int moveX, int moveY) {
            CheckCollision(moveX, moveY);
            Tile? nextCell = _map?.GetTileAt(_transform.X + moveX, _transform.Y + moveY);
            if (nextCell is {IsObstacle: false}) {
                _transform.X += moveX;
                _transform.Y += moveY;
            }
        }

        public void CheckCollision(int moveX, int moveY) {
            moveX += _transform.X;
            moveY += _transform.Y;
            Tile? tileBeingWalkedOn = _map?.GetTileAt(moveX, moveY);

            if (tileBeingWalkedOn is {IsLife: true}) {
                tileBeingWalkedOn.Sprite = "  ";
                AddHealth();
            }
        }

        public string Sprite {
            get => _sprite;
            set => _sprite = value ?? throw new ArgumentNullException(nameof(value));
        }
    }
}