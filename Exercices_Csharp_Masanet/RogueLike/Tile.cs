namespace Masanet_console_1.RogueLike {
    public class Tile {
        private Vector2 _transform;

        public Tile(int x, int y, string sprite, bool isObstacle = false, bool isLife = false) {
            _transform = new Vector2(x, y);
            Sprite = sprite;
            IsObstacle = isObstacle;
        }

        public Vector2 Transform {
            get => _transform;
            set => _transform = value ?? throw new ArgumentNullException(nameof(value));
        }

        public string Sprite { get; set; }
        public bool IsObstacle { get; set; }
        public bool IsLife { get; set; }
    }
}