namespace Masanet_console_1.RogueLike {
    public static class RogueLike {
        private static bool _gameOver = false;
        private static Hero? _hero;
        private static Map? _map;

        public static void Start() {
            Console.Clear();

            // Création du héros
            _hero = new Hero(3, 4);

            // Création de l'objet de la map
            _map = new Map(_hero);

            // Assigne la map au héros pour pouvoir gérer le déplacement
            _hero.Map = _map;

            // Génération générique de la map
            var random = new Random();
            int randomMapWidth = random.Next(30, 50);
            int randomMapHeight = random.Next(10, 30);
            _map.GenerateBlankMap(randomMapWidth, randomMapHeight);
            _map.RemoveOrphanTiles();
            _map.GenerateLifeItems();

            while (!_gameOver) {
                Console.CursorVisible = false;
                // Affichage des tiles
                _map.DrawTiles();
                // Affichage des stats du Héros
                _hero.DrawStats();
                HandleInput(_hero);
            }
        }

        private static void HandleInput(Hero? hero) {
            var input = Console.ReadKey().Key;

            switch (input) {
                case ConsoleKey.UpArrow:
                    hero?.Move(0, -1);
                    break;
                case ConsoleKey.RightArrow:
                    hero?.Move(1, 0);
                    break;
                case ConsoleKey.DownArrow:
                    hero?.Move(0, 1);
                    break;
                case ConsoleKey.LeftArrow:
                    hero?.Move(-1, 0);
                    break;
                case ConsoleKey.A:
                    hero?.SubHealth();
                    break;
            }
        }

        public static void GameOver() {
            _gameOver = true;
            _hero?.UpdateSprite();
            _map?.DrawTiles();
            Console.WriteLine("\nPerdu !");
        }
    }
}