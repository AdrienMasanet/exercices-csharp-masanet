namespace Masanet_console_1.RogueLike {
    public class Map {
        private readonly List<Tile?> _tiles = new List<Tile?>();
        private readonly Hero? _hero;

        public Map(Hero? hero) {
            _hero = hero;
        }

        public void GenerateBlankMap(int tilesX, int tilesY, int lifeCount = 3, int emptinessChance = 15) {
            // Instantiation des tiles et assignation à la map
            for (int y = 0; y < tilesY; y++) {
                for (int x = 0; x < tilesX; x++) {
                    if (y == 0 || x == 0 || y == (tilesY - 1) || x == (tilesX - 1)) {
                        _tiles.Add(new Tile(x, y, "🧱", true));
                    }
                    else {
                        var randomWillBeObstacle = new Random();
                        int willBeObstacle = randomWillBeObstacle.Next(emptinessChance);
                        if (willBeObstacle < (emptinessChance / 5)) {
                            _tiles.Add(new Tile(x, y, "🧱", true));
                        }
                        else {
                            _tiles.Add(new Tile(x, y, "  ️"));
                        }
                    }
                }
            }
        }

        public void GenerateLifeItems(int lifeCount = 3) {
            while (lifeCount > 0) {
                foreach (var tile in _tiles) {
                    if (tile != null && !tile.IsObstacle) {
                        var randomWillBeLife = new Random();
                        int willBeLife = randomWillBeLife.Next(_tiles.Count);
                        if (willBeLife == 0) {
                            tile.Sprite = "💖️";
                            tile.IsLife = true;
                            lifeCount--;
                        }
                    }
                }
            }
        }

        public void DrawTiles() {
            var enumerable = _tiles;
            int lastTilesY = 0;
            string finalOutput = "";

            Console.Clear();

            _hero?.UpdateSprite();

            foreach (var tile in enumerable) {
                if (tile != null && tile.Transform.Y > lastTilesY) {
                    finalOutput += "\n";
                }

                // Si il y a une tile à cet emplacement dans le tableau, on Write son sprite
                // Sinon, on Write un espace pour faire du vide
                if (tile != null) {
                    if (_hero != null && _hero.Transform.X == tile.Transform.X && _hero.Transform.Y == tile.Transform.Y) {
                        finalOutput += _hero.Sprite;
                    }
                    else {
                        finalOutput += tile.Sprite;
                        lastTilesY = tile.Transform.Y;
                    }
                }
                else {
                    finalOutput += " ";
                }
            }

            Console.Write(finalOutput);
        }

        public Tile? GetTileAt(int x, int y) {
            foreach (var tile in _tiles) {
                if (tile != null && tile.Transform.X == x && tile.Transform.Y == y) {
                    return tile;
                }
            }

            return null;
        }

        public void RemoveOrphanTiles() {
            foreach (var tile in _tiles) {
                if (
                    tile != null &&
                    (GetTileAt(tile.Transform.X - 1, tile.Transform.Y) != null &&
                     GetTileAt(tile.Transform.X - 1, tile.Transform.Y)!.IsObstacle) &&
                    (GetTileAt(tile.Transform.X + 1, tile.Transform.Y) != null &&
                     GetTileAt(tile.Transform.X + 1, tile.Transform.Y)!.IsObstacle) &&
                    (GetTileAt(tile.Transform.X, tile.Transform.Y + 1) != null &&
                     GetTileAt(tile.Transform.X, tile.Transform.Y + 1)!.IsObstacle) &&
                    (GetTileAt(tile.Transform.X, tile.Transform.Y - 1) != null &&
                     GetTileAt(tile.Transform.X, tile.Transform.Y - 1)!.IsObstacle)
                ) {
                    tile.Sprite = "🧱";
                    tile.IsObstacle = true;
                }
            }
        }
    }
}